//actions
import jsonPlaceholder from '../apis/jsonPlaceHolder.js';
import _ from 'lodash';


//redux-thunk allows us to return an async action creator 
const fetchPosts = () =>{
     return async (dispatch) =>{ //redux thunk 
        const response = await jsonPlaceholder.get('/posts');
        dispatch({ type: 'FETCH_POSTS', payload: response.data });
     };
};

const fetchUser = (id) =>{
    return async (dispatch) =>{
       const response = await jsonPlaceholder.get(`/users/${id}`);
       dispatch({ type: 'FETCH_USER', payload: response.data });
    };
};

export const fetchPostsAndUsers = () =>{
    return async (dispatch, getState) =>{
        await dispatch(fetchPosts());
        const userIds = _.uniq(_.map(getState().posts, 'userId')); //10 unique user id hence only 100 fetchUser executed
        userIds.forEach(id => dispatch(fetchUser(id)));
    };
};





