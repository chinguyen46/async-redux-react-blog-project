import React from 'react';
import { connect } from 'react-redux';
import { fetchPostsAndUsers } from '../actions/index.js';
import UserHeader from './UserHeader.js';

class PostList extends React.Component {

    componentDidMount(){
        this.props.fetchPostsAndUsers();
    }

    renderList(){
        return this.props.posts.map(post => {
            return (
                <div className="list-group-item" key={post.id}>
                    <i className="fas fa-2x fa-user float-left"></i>
                    <div className="content">
                        <div>
                            <h2>{post.title}</h2>
                            <p>{post.body}</p>
                        </div>
                        <UserHeader userId={post.userId} />
                    </div>
                </div>
            )
        });
    }


    render(){
        return (
        <div>
           <div className="list-group">{this.renderList()}</div>
        </div>
        )
    }
};


const mapStateToProps = (state) =>{
    return { posts: state.posts };
};

export default connect(mapStateToProps, {
    fetchPostsAndUsers: fetchPostsAndUsers
})(PostList);