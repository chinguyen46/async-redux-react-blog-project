import React from 'react';
import { connect } from 'react-redux';


class UserHeader extends React.Component {

    render(){
        const { user } = this.props;
        if(!user){
            return null;
        }else{
            return (
            <div className="header">{user.name}</div>
            )
        }
    }
};

const mapStateToProps = (state, ownProps) =>{ //second argument ownProps allow us to use logic 
    return { user: state.users.find(user => user.id === ownProps.userId) };
};

export default connect(mapStateToProps)(UserHeader);